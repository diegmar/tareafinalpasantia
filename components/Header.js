import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image }from 'react-native';

const Header = () => {
    return(
        <View style={styles.header}>
            <View style={styles.iconleft}>
                <TouchableOpacity>
                  <Image source={require('../assets/camera.png')} style={styles.Icam}/>
                </TouchableOpacity>
            </View>
            <View style={styles.iconcenter}>
                <TouchableOpacity style={{width: '100%'}}>
                  <Image source={require('../assets/title.png')} style={styles.Ititle}/>
                </TouchableOpacity>
            </View>
            <View style={styles.iconleft}>
                <TouchableOpacity>
                  <Image source={require('../assets/share1.png')} style={styles.Ishare}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor: '#F0F0F0',
    },
    iconleft: { 
        flex: 0.15, 
        width: '100%', 
        height: '100%',
        padding: 5,
    },
    iconright: {
        flex: 0.15, 
        width: '100%', 
        height: '100%',
        padding: 5,
    },
    iconcenter: {
        flex: 0.7, 
        width: '100%', 
        height: '100%',
        alignItems: 'center',
    },
    Icam: {
        width: 35, 
        height: 35, 
        margin: 4 
    },
    Ititle: {
        width: 145, 
        height: '100%', 
    },
    Ishare: {
        width: '100%', 
        height: '100%', 
    },
});

export default Header;