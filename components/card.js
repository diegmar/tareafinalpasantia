import React from 'react';
import { View, StyleSheet, Text, Image, TextInput} from 'react-native';
import Slider from './Slider'
import Swiper from 'react-native-swiper'
import CardHead from './CardHead'
import Footer from './Footer'

function Card ({ item }) {

    return(
        <View style= {styles.card}>
            <CardHead 
            title={item.title} 
            subtitle={item.subtitle} 
            profile={item.profile}/>
            <Swiper>{item.imagenes.map((item) => <Slider uri={item.image}/>)}</Swiper>
            <Footer 
            title={item.title} 
            description={item.description} 
            likes={item.likes}/>
        </View>
    )
    
}

const styles = StyleSheet.create({
    card: {
        overflow: 'hidden',
        backgroundColor: 'white',
        height:500,
      }
    
})


export default Card;