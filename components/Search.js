import React, {useState, useEffect} from 'react';
import axios from 'axios'
import { View , SafeAreaView, StyleSheet, FlatList, Image } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: 'white',
	},
	imageThumbnail: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 100,
	},
});

const Search = () => {

	const [dataSource, setDataSource] = useState([]);
	const fetchData = () => {
		axios
			.get('https://picsum.photos/v2/list')
			.then((response) => {					
				setDataSource(response.data)
		  })
		  .catch((err) => console.log(err));
	  };

	useEffect(() => fetchData(), []);

	return (
		<SafeAreaView style={styles.container}>
			<FlatList
				data={dataSource}
				renderItem={({item}) => (
					<View
						style={{
							flex: 1,
							flexDirection: 'column',
							margin: 1
						}}>
						<Image
							style={styles.imageThumbnail}
							source={{uri: 'https://picsum.photos/id/' + item.id + '/100/100'}}
						/>
					</View>
				)}
				//Setting the number of column
				numColumns={3}
				keyExtractor={(item, index) => index}
			/>
		</SafeAreaView>
	);
};
export default Search;