import * as React from 'react';
import { View, ScrollView, StyleSheet, FlatList} from 'react-native';
import Card from './card';
import Header from './Header';
import Stories from './stories';


function Home(){

    const imagesSlider = [
                
                {
                    id:1,
                    imagenes: [
                        {image:require('../assets/acdc3.jpg')},
                        {image:require('../assets/acdc4.jpg')},
                        {image:require('../assets/acdc5.jpg')}
                    ],
                    title: "acdc",
                    subtitle: "Band",
                    profile: require('../assets/acdcprofile.jpg'),
                    likes: "20,923,789",
                    description: "AC/DC is an abbreviation meaning alternating current/direct current electricity. The brothers felt that this name symbolized the band's raw energy, power-driven performances of their music."
                },
                {
                    id:2,
                    imagenes: [
                        {image:require('../assets/postMalone3.jpg')},
                        {image:require('../assets/postMalone2.jpg')},
                        {image:require('../assets/postMalone1.jpg')},
                        {image:require('../assets/postMalone4.jpg')},
                        {image:require('../assets/postMalone5.jpg')}
                    ],
                    title: "postmalone",
                    subtitle: "Artist",
                    profile: require('../assets/postMalone.jpg'),
                    likes: "1,234,590",
                    description: "Post Malone known professionally as Post Malone, is an American singer, rapper, songwriter, record producer, and actor. Known for his introspective songwriting and laconic vocal style"
                },
                {
                    id:3,
                    imagenes: [
                        {image:require('../assets/drake.jpg')},
                        {image:require('../assets/drake2.jpg')},
                        {image:require('../assets/drake1.jpg')},
                        {image:require('../assets/drake3.jpg')}
                    ],
                    title: "champagnepapi",
                    subtitle: "Artist",
                    profile: require('../assets/drakeProfile.jpg'),
                    likes: "9,923,789",
                    description: "Aubrey Drake Graham (born October 24, 1986) is a Canadian rapper, singer, songwriter, actor, record producer, and entrepreneur. A prominent figure in popular music, Drake is credited for popularizing the Toronto sound."
                },
                {
                    id:4,
                    imagenes: [
                        {image:require('../assets/travis4.jpg')},
                        {image:require('../assets/travisscott.jpg')},
                        {image:require('../assets/travis2.jpg')},
                        {image:require('../assets/travis3.jpg')}
                    ],
                    title: "travisscott",
                    subtitle: "Artist",
                    profile: require('../assets/travisprofile.jpg'),
                    likes: "2,923,789",
                    description: "Jacques Berman Webster II (born April 30, 1991), known professionally as Travis Scott (formerly stylized as Travi$ Scott), is an American rapper, singer, songwriter, and record producer."
                },
                {
                    id:5,
                    imagenes: [
                        {image:require('../assets/chilipepers.jpg')},
                        {image:require('../assets/chilipepers2.jpg')},
                        {image:require('../assets/chilipepers3.jpg')}
                    ],
                    title: "chilipepers",
                    subtitle: "Band",
                    profile: require('../assets/chiliprofile.png'),
                    likes: "22,943,719",
                    description: "Red Hot Chili Peppers are an American rock band formed in Los Angeles in 1983. Their music incorporates elements of alternative rock, funk, punk rock and psychedelic rock."
                },
                {
                    id:6,
                    imagenes: [
                        {image:require('../assets/tomorrowland.jpg')},
                        {image:require('../assets/tomorrowland2.jpg')},
                        {image:require('../assets/tomorrowland3.jpg')}
                    ],
                    title: "tomorrowland",
                    subtitle: "Event",
                    profile: require('../assets/tomorrowprofile.jpg'),
                    likes: "20,923,789",
                    description: "Tomorrowland is a Belgian electronic dance music festival held in Boom, Belgium. Tomorrowland was first held in 2005. It now stretches over two weekends and usually sells out in minutes."
                }
            ]
   
            const itemS = [
                {
                    id: 1,
                    nombre: 'Usuario A.',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_16.jpg?itok=iohCz0oN',
                    foto: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_15.jpg?itok=mdlW5PrX',
                },
                 {
                    id: 2,
                    nombre: 'Anonimo',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_19.jpg?itok=Gvv_GabL',
                    foto: 'https://i.pinimg.com/originals/20/0c/95/200c955ac9d1ec4ca815ebf351e7b2bc.jpg',
                },
                {
                    id: 3,
                    nombre: 'JS name',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_13.jpg?itok=8b-joAiZ',
                    foto: '',
                },
                {
                    id: 4,
                    nombre: 'XD Us',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_1.jpg?itok=4bKgc-m6',
                    foto: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUIfW4L53sqrl00MRATU6ksdPi8GB9zZ_9iQ&usqp=CAU',
                },
                {
                    id: 5,
                    nombre: 'Bananas',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp.jpg?itok=KqAMbYJr',
                    foto: 'https://platinoweb.com/images/material_de_consulta/social_media/tips-para-tomar-las-mejores-fotos-de-instagram.jpg',
                },
                {
                    id: 6,
                    nombre: 'XD Anonim',
                    perfil: 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/08/fotos-perfil-whatsapp_18.jpg?itok=3bs8L4IS',
                    foto: 'https://i.pinimg.com/736x/1e/f4/14/1ef414f7fb69e36b95411d8efa02c9f5.jpg',
                },
                {
                    id: 7,
                    nombre: 'LiLi',
                    perfil: 'https://latravelista.com/wp-content/uploads/2018/04/frases-para-instagram-captan-atencion.jpg',
                    foto: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQVgSpn49ZPgCSzdwHEPqdhZOyWmE7E4MI7VA&usqp=CAU',
                },
                
                {
                    id: 8,
                    nombre: 'Amigo1',
                    perfil: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTsgjoTdxdF4AakICZrGu7lcXIFwZNpozIglw&usqp=CAU',
                    foto: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSXMOdU7rcr8cru7BF4Yn--6SmdzavVnDwrsQ&usqp=CAU',
                },
            ];
        

	return (
		<View style={styles.container}>
            <Header/>
            <ScrollView style={{flex:1}}>
                <View style={styles.storiesheader}>
                    <FlatList
                        renderItem={({item}) => 
                            <Stories 
                                name={item.nombre}
                                perfil={item.perfil}
                                image={item.foto} />}
                            data={itemS}
                        keyExtractor={(item) => String(item.id)}
                        horizontal = {true}
                    />
                </View>
               {imagesSlider.map((item) => <Card item={item}/>)}
            </ScrollView>
        </View>
	);
}

const styles = StyleSheet.create({
	container:{
		flex:1, 
	},
})

export default Home;