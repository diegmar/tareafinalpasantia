import React from 'react';
import { View, StyleSheet, Text, Image} from 'react-native';


function CardHead ({profile, title, subtitle}) {

    return(
        <View style={styles.container}>
            <Image 
            style={styles.profile} 
            source={profile} />
        <View style={styles.infoProfile}>
            <Text 
            style={styles.title}>{title}</Text>
            <Text 
            style={styles.subtitle}>{subtitle}</Text>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    container: {
        width:'100%', 
        height:60, 
        alignItems:'center', 
        paddingLeft:10, 
        flexDirection:'row'
    },
    profile: { 
        width:40, 
        height:40, 
        borderRadius:20
    },
    infoProfile: {
        paddingLeft:10
    },
    title: {
        fontSize:15,
        fontWeight:'bold'
    },
    subtitle: {
        fontSize:10
    },
})

export default CardHead;