import * as React from 'react';
import { View } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import { Avatar, Accessory } from 'react-native-elements';

function MyTabBar({ navigation }) {
	return (
		<View style={{
					padding: 10,
					flexDirection: 'column',
					justifyContent: 'space-around',
					flexDirection: 'row'}
				}>
			<Icon
				name    = "home"
				color   = '#000000'
				size    = {25}
				onPress = {()=>navigation.navigate('Home')}/>

			<Icon
				name="search"
				color='#000000'
				size={25}
				onPress = {()=>navigation.navigate('Search')}/>
			
			<Icon
				name="add-circle"
				color='#000000'
				size={25}/>
			
			<Icon
				name="heart-sharp"
				color='#000000'
				size={25}
			/>
			<Avatar
				rounded
				size={25}
				source={{
					uri:
					'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
				}}		
			/>
		</View>
	);
}
export default MyTabBar;