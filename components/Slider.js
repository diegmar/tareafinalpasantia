import React from 'react'
import {
    View,
    Image,
    Dimensions,
    StyleSheet
} from 'react-native'

const {width} = Dimensions.get('window')

const Slider = (props) =>{
    return(
        <View style={styles.container}>
            <Image 
            style={styles.image} 
            source={props.uri}/>
        </View>
    )
}
    
const styles = StyleSheet.create({
    container: {
        height:300,
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        width
    }  
})

export default Slider;

