import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Text, ScrollView, FlatList}from 'react-native';

const Header = (props) => {
    return(
        <View>
            <TouchableOpacity style={styles.stories}>
                <Image
                    style={styles.storiecar}
                    source={{ uri:props.perfil}}
                />
                <Text style={styles.user}>{props.name}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    stories:{
        width: 80,
        height: 90,
        //margin: 5,
        alignItems: 'center',
        paddingTop: 5,
    },
    storiecar: {
        width: 60, 
        height: 60,
        borderRadius:30,
        borderColor: 'red',
    },
    user: {
        color: 'black',
        fontSize: 13,
        paddingTop: 5,
    },
});

export default Header;