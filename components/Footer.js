import React, { useState } from 'react';
import { View, StyleSheet, Text, Image, TextInput} from 'react-native';
import { TouchableOpacity } from 'react-native';

function Footer ({description, likes}) {
    const [reaction, setReaction] = useState(false)

    return(
        <View>
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>setReaction(!reaction)}>
                    {reaction ?
                    <Image 
                    style={styles.like} 
                    source={require('../assets/like4.png')} /> 
                    :
                    <Image 
                    style={styles.like} 
                    source={require('../assets/like2.jpg')} />}
                </TouchableOpacity> 
                    <Image 
                    style={{width:55, height:55}} 
                    source={require('../assets/coment.png')} />
                    <Image 
                    style={styles.icon} 
                    source={require('../assets/share1.png')} />
                <View style={styles.favorites}>
                    <Image 
                    style={styles.icon} 
                    source={require('../assets/favoritos.png')} />
                </View>
            </View>
            <View style={styles.reactions}>
                {reaction ?
                <Text 
                style={styles.bold}>Liked you and {likes} others. </Text> 
                :
                <Text 
                style={styles.bold} >Liked {likes} others. </Text>}
            </View>
            <View style={styles.description}>
                <Text>{description}</Text>
            </View>
            <View style={styles.textInput}>
                <TextInput placeholder={'Add a comment'}></TextInput>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height:50, 
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center', 
        paddingLeft:10
    },
    like: {
        width:30, 
        marginRight:1, 
        height:30
    },
    favorites: {
        alignItems:'flex-end', 
        flex:1
    },
    icon: {
        width:40, 
        height:40
    },
    reactions: {
        width:'auto', 
        height:20, 
        flexDirection:'row', 
        marginLeft:10
    },
    bold: {
        fontWeight:'bold'
    },
    description: {
        marginHorizontal:10, 
        flexDirection:'row'
    },
    textInput: {
        width:'100%', 
        height:40, 
        marginLeft:10
    }
})

export default Footer;