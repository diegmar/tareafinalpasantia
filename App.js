import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home     from './components/Home'
import Search   from './components/Search'
import MyTabBar from './components/MyTabBar'

const Tab = createBottomTabNavigator();

function App () {
	return(
		<NavigationContainer>
			<Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
				<Tab.Screen name="Home"   component = { Home } />
				<Tab.Screen name="Search" component = { Search } />
			</Tab.Navigator>
		</NavigationContainer>
	)
}

export default App;